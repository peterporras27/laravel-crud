<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/pronummus', 'ApiController@pronomous');

Route::group(['middleware' => 'auth:api'], function () 
{
    Route::post('logout', 'Auth\LoginController@logout');
    Route::get('/user', 'Auth\UserController@current');

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');
    
    Route::get('getroles', 'PublicController@getRoles');
    Route::get('getcities', 'PublicController@getCities');
    Route::get('getfacilities', 'PublicController@getfacilities');
    Route::get('getemployees', 'PublicController@getEmployees');
    Route::get('getcourses', 'PublicController@getCourses');
    Route::get('getcategories', 'PublicController@getCategories');
    Route::get('getranks', 'PublicController@getRanks');
    Route::get('getdivisions', 'PublicController@getDivisions');

    Route::post('users/import', 'API\UserController@csvupload');
    Route::post('users/upload/image', 'API\UserController@imageUpload');
    Route::post('users/{id}/upload', 'API\UserController@profileImgUpload');

    Route::get('trainees/{id}', 'API\UserController@getTrainees');
    Route::post('trainees/{id}/assign', 'API\UserController@assignTrainees');
    Route::get('courses/{id}/overview', 'API\CourseController@overview');
    Route::get('mytrainees/{id}', 'HomeController@getCustomerTrainees');
    Route::get('getorders', 'HomeController@getOrders');
    
    Route::apiResources([
        'users' => 'API\UserController',
        'cities' => 'API\CityController',
        'facilities' => 'API\FacilityController',
        'courses' => 'API\CourseController',
        'ranks' => 'API\RankController',
        'categories' => 'API\CategoryController',
        'exams' => 'API\ExamController',
        'schedules' => 'API\ScheduleController',
        'divisions' => 'API\DivisionController',
        'traininglogs' => 'API\TrainingLogsController'
    ]);

    Route::post('exam/take', 'API\UserController@takethetest');
    Route::get('exam/logs', 'HomeController@examlogs');
    Route::patch('exam/logs/{id}', 'HomeController@updateExamLogs');

    Route::patch('orders/{id}', 'HomeController@updateOrder');

    Route::get('activities', 'API\LaravelLoggerController@showAccessLog');
    Route::get('activities/cleared', 'API\LaravelLoggerController@showClearedActivityLog');
    Route::delete('activities/delete', 'API\LaravelLoggerController@clearActivityLog');
    Route::get('activities/restore', 'API\LaravelLoggerController@restoreClearedActivityLog');
    Route::delete('activities/softdelete', 'API\LaravelLoggerController@destroyActivityLog');
});

Route::group(['middleware' => 'guest:api'], function () {
    
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');

});
