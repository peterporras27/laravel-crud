<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ranks', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->integer('rank')->nullable(); 
            $table->string('color')->nullable();
            $table->integer('duration')->nullable(); // in months
            $table->enum('duration_type',['days','weeks','months','years'])->default('months'); // in months
            $table->integer('max_points')->nullable(); // rank points limitation
            $table->integer('division_id')->nullable(); // rank division
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ranks');
    }
}
