<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRankUpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank_ups', function (Blueprint $table) {
            $table->id();
            $table->enum('status',['hold','processing','success'])->default('hold');
            $table->integer('user_id')->nullable();
            $table->integer('prev_rank_id')->nullable();
            $table->integer('rank_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rank_ups');
    }
}
