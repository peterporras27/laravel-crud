<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->default('');
            $table->string('last_name')->default('');
            $table->string('email',191)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('street')->nullable();
            $table->string('street_no')->nullable();
            $table->string('zip')->nullable();
            $table->string('city')->nullable();
            $table->string('phone')->nullable();
            $table->string('bank_iban')->nullable();
            $table->string('bank_bic')->nullable();

            // For API
            $table->string('uid')->nullable();
            $table->string('membership_number')->nullable();
            $table->string('status')->nullable();
            $table->string('salutation')->nullable();
            $table->string('residence')->nullable();
            $table->string('address')->nullable();
            $table->string('birthday')->nullable();
            $table->string('age')->nullable();
            $table->string('initial_contract')->nullable();
            $table->string('actual_contract')->nullable();
            $table->string('duration')->nullable();
            $table->string('end')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('contribution')->nullable();
            $table->string('booking_day')->nullable();
            $table->string('admission')->nullable();
            $table->string('cycle')->nullable();
            $table->string('termination_date')->nullable();
            $table->string('iban')->nullable();
            
            $table->string('image')->nullable();
            $table->string('card_id')->nullable(); // This will be used for membercard
            $table->integer('course_id')->nullable(); // for Employees and Trainees
            $table->integer('customer_id')->nullable(); // parent

            $table->integer('points')->default(0); // 
            $table->date('start_date')->nullable(); // course start date // Adjustable
            $table->date('end_date')->nullable(); // course end date // Non Adjustable
            $table->integer('rank_id')->nullable(); // rank depending on course.

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
