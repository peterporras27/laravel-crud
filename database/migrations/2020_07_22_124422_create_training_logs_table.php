<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_logs', function (Blueprint $table) {
            $table->id();
            $table->string('login_time')->nullble();
            $table->integer('course_id')->nullble();
            $table->integer('user_id')->nullble(); // Trainee ID
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_logs');
    }
}
