<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\User;
use App\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_role = Role::where('slug','admin')->first();
        $employee_role = Role::where('slug','employee')->first();

        $admin = new User();
        $admin->first_name = 'Administrator';
        $admin->last_name = '';
        $admin->email = 'admin@admin.com';
        $admin->password = app('hash')->make('secret');
        $admin->save();
        $admin->roles()->attach($admin_role);

        $employee = new User();
        $employee->first_name = 'Employee';
        $employee->last_name = '';
        $employee->email = 'employee@employee.com';
        $employee->password = app('hash')->make('secret');
        $employee->save();
        $employee->roles()->attach($employee_role);

        foreach ($admin_role->permissions()->get() as $perm) 
        {
            $admin->permissions()->attach($perm);
        }

        foreach ($employee_role->permissions()->get() as $emperm) 
        {
            $employee->permissions()->attach($emperm);
        }
    }
}