<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	['name'=>'Self Defence'],
        	['name'=>'Combination'],
        	['name'=>'Break Test'],
        	['name'=>'Fitness Test']
        ]);
    }
}
