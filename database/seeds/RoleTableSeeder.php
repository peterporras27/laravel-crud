<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$permissions = Permission::all();

        $admin_role = new Role();
		$admin_role->slug = 'admin';
		$admin_role->name = 'Administrator';
		$admin_role->save();

        foreach ($permissions as $perm) 
        {
            $admin_role->permissions()->attach($perm);   
        }

        $employee_role = new Role();
        $employee_role->slug = 'employee';
        $employee_role->name = 'Employee';
        $employee_role->save();

        $employee = array(
            'manage-course',
            'edit-course',
            'create-course',
            'update-course',
            'delete-course',

            'manage-member-card',
            'edit-member-card',
            'create-member-card',
            'update-member-card',
            'delete-member-card',

            'manage-exam',
            'edit-exam',
            'create-exam',
            'update-exam',
            'delete-exam',

            'manage-schedule',
            'edit-schedule',
            'create-schedule',
            'update-schedule',
            'delete-schedule',

            'manage-division',
            'edit-division',
            'create-division',
            'update-division',
            'delete-division'
        );

        $employee_permissions = Permission::whereIn('slug',$employee)->get();

        foreach ($employee_permissions as $em_perm) 
        {
            $employee_role->permissions()->attach($em_perm);   
        }

        $customer_role = new Role();
        $customer_role->slug = 'customer';
        $customer_role->name = 'Customer';
        $customer_role->save();

        $trainee_role = new Role();
        $trainee_role->slug = 'trainee';
        $trainee_role->name = 'Trainee';
        $trainee_role->save();
    }
}
