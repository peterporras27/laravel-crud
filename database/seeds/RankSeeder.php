<?php

use Illuminate\Database\Seeder;

class RankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ranks')->insert([
        	['name'=>'White','rank'=>0,'color'=>'','duration'=>3,'max_points'=>30],
        	['name'=>'White Yellow','rank'=>1,'color'=>'','duration'=>3,'max_points'=>32],
        	['name'=>'Yellow','rank'=>2,'color'=>'','duration'=>3,'max_points'=>34],
        	['name'=>'Yellow-green','rank'=>3,'color'=>'','duration'=>4,'max_points'=>40],
        	['name'=>'Green','rank'=>4,'color'=>'','duration'=>4,'max_points'=>44],
        	['name'=>'Green Blue','rank'=>5,'color'=>'','duration'=>4,'max_points'=>48],
        	['name'=>'Blue','rank'=>6,'color'=>'','duration'=>5,'max_points'=>50],
        	['name'=>'Blue Red','rank'=>7,'color'=>'','duration'=>6,'max_points'=>60],
        	['name'=>'Red','rank'=>8,'color'=>'','duration'=>10,'max_points'=>100],
        	['name'=>'Red Black','rank'=>9,'color'=>'','duration'=>0,'max_points'=>0],

            ['name'=>'White Yellow White','rank'=>10,'color'=>'','duration'=>4,'max_points'=>40],
            ['name'=>'White Green White','rank'=>11,'color'=>'','duration'=>5,'max_points'=>50],
            ['name'=>'White Blue White','rank'=>12,'color'=>'','duration'=>6,'max_points'=>60],
            ['name'=>'White Red White','rank'=>13,'color'=>'','duration'=>7,'max_points'=>70],
            ['name'=>'White Black White','rank'=>14,'color'=>'','duration'=>0,'max_points'=>0]
        ]);
    }
}
