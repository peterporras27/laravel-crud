<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin permissions
		DB::table('permissions')->insert([
            
            ['slug' => 'manage-user', 'name' => 'Manage all users'],
            ['slug' => 'edit-user', 'name' => 'Edit user'],
            ['slug' => 'create-user', 'name' => 'Create new user'],
            ['slug' => 'update-user', 'name' => 'Update user'],
			['slug' => 'delete-user', 'name' => 'Delete a user'],

            ['slug' => 'manage-role','name' => 'Manage all roles'],
            ['slug' => 'edit-role','name' => 'Can edit role'],
            ['slug' => 'create-role','name' => 'Can create a role'],
            ['slug' => 'update-role','name' => 'Can update a role'],
			['slug' => 'delete-role','name' => 'Can delete a role'],
            
            ['slug' => 'manage-city','name' => 'Manage all cities'],
            ['slug' => 'edit-city','name' => 'Edit a city'],
            ['slug' => 'create-city','name' => 'Create a city'],
            ['slug' => 'update-city','name' => 'Update a city'],
            ['slug' => 'delete-city','name' => 'Delete a city'],

            ['slug' => 'manage-facility','name' => 'Manage facility'],
            ['slug' => 'edit-facility','name' => 'Can edit facility'],
            ['slug' => 'create-facility','name' => 'Can create facility'],
            ['slug' => 'update-facility','name' => 'Can update facility'],
            ['slug' => 'delete-facility','name' => 'Can delete a facility'],

            ['slug' => 'manage-course','name' => 'Manage course'],
            ['slug' => 'edit-course','name' => 'Can edit course'],
            ['slug' => 'create-course','name' => 'Can create course'],
            ['slug' => 'update-course','name' => 'Can update course'],
            ['slug' => 'delete-course','name' => 'Delete course'],

            ['slug' => 'manage-member-card','name' => 'Manage member card'],
            ['slug' => 'edit-member-card','name' => 'Edit member card'],
            ['slug' => 'create-member-card','name' => 'Create member card'],
            ['slug' => 'update-member-card','name' => 'Update member card'],
            ['slug' => 'delete-member-card','name' => 'Delete member card'],

            ['slug' => 'manage-exam','name' => 'Manage exam'],
            ['slug' => 'edit-exam','name' => 'Edit exam'],
            ['slug' => 'create-exam','name' => 'Create exam'],
            ['slug' => 'update-exam','name' => 'Create exam'],
            ['slug' => 'delete-exam','name' => 'Delete exam'],

            ['slug' => 'manage-rank','name' => 'Manage rank'],
            ['slug' => 'edit-rank','name' => 'Edit rank'],
            ['slug' => 'create-rank','name' => 'Create rank'],
            ['slug' => 'update-rank','name' => 'Create rank'],
            ['slug' => 'delete-rank','name' => 'Delete rank'],

            ['slug' => 'manage-category','name' => 'Manage category'],
            ['slug' => 'edit-category','name' => 'Edit category'],
            ['slug' => 'create-category','name' => 'Create category'],
            ['slug' => 'update-category','name' => 'Create category'],
            ['slug' => 'delete-category','name' => 'Delete category'],

            ['slug' => 'manage-schedule','name' => 'Manage schedule'],
            ['slug' => 'edit-schedule','name' => 'Edit schedule'],
            ['slug' => 'create-schedule','name' => 'Create schedule'],
            ['slug' => 'update-schedule','name' => 'Create schedule'],
            ['slug' => 'delete-schedule','name' => 'Delete schedule'],

            ['slug' => 'manage-division','name' => 'Manage division'],
            ['slug' => 'edit-division','name' => 'Edit division'],
            ['slug' => 'create-division','name' => 'Create division'],
            ['slug' => 'update-division','name' => 'Create division'],
            ['slug' => 'delete-division','name' => 'Delete division']
    	]);
    }
}
