var staticCacheName = "pwa-v" + new Date().getTime();
var filesToCache = [
    // '/login',
    // '/register',
    // '/home',
    // '/css/all.css',
    // '/js/all.js',
    // '/hdlogo.png',
    // '/images/icons/icon-72x72.png',
    // '/images/icons/icon-96x96.png',
    // '/images/icons/icon-128x128.png',
    // '/images/icons/icon-144x144.png',
    // '/images/icons/icon-152x152.png',
    // '/images/icons/icon-192x192.png',
    // '/images/icons/icon-384x384.png',
    // '/images/icons/icon-512x512.png',

    // '/css/icons/themify-icons/themify-icons.css',
    // '/css/icons/material-design-iconic-font/css/materialdesignicons.min.css',
    // '/css/icons/material-design-iconic-font/fonts/materialdesignicons-webfont.woff2?v=1.8.36',
    // '/css/icons/weather-icons/css/weather-icons.min.css',

    'https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900',
];

// var isOnline = window.navigator.onLine;

// Cache on install
self.addEventListener("install", event => {
    // console.log( '********** START: Cache on install' );
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    )
});

// Clear cache on activate
self.addEventListener('activate', event => {
    // console.log( '********** START: Clear cache on activate' );
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("pwa-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Serve from Cache
self.addEventListener("fetch", event => {
    // console.log( '********** START: Serve from Cache' );
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                return response || fetch(event.request);
            })
            .catch(() => {
                return caches.match('home');
            })
    )
});