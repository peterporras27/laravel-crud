function page (path) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [
  { path: '/', redirect: { name: 'login' } },
  { path: '/login', name: 'login', component: page('auth/login.vue') },
  { path: '/register', name: 'register', component: page('auth/register.vue') },
  { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },
  { path: '/profile/:id', name: 'users.profile', component: page('users/profile.vue') },
  { path: '/home', name: 'home', component: page('home.vue') },
  { path: '/users',
    component: page('users/index.vue'),
    children: [
      { path: '', redirect: { name: 'users.list' } },
      { path: 'list', name: 'users.list', component: page('users/list.vue') },
      { path: 'import', name: 'users.import', component: page('users/import.vue') },
      { path: 'create', name: 'users.create', component: page('users/create.vue') }
    ] },
  { path: '/cities',
    component: page('cities/index.vue'),
    children: [
      { path: '', redirect: { name: 'cities.list' } },
      { path: 'list', name: 'cities.list', component: page('cities/list.vue') },
      { path: 'create', name: 'cities.create', component: page('cities/create.vue') }
    ] },
  { path: '/facilities',
    component: page('facilities/index.vue'),
    children: [
      { path: '', redirect: { name: 'facilities.list' } },
      { path: 'list', name: 'facilities.list', component: page('facilities/list.vue') },
      { path: 'create', name: 'facilities.create', component: page('facilities/create.vue') }
    ] },
  { path: '/courses',
    component: page('courses/index.vue'),
    children: [
      { path: '', redirect: { name: 'courses.list' } },
      { path: 'list', name: 'courses.list', component: page('courses/list.vue') },
      { path: 'create', name: 'courses.create', component: page('courses/create.vue') }
    ] },
  { path: '/overview/:id',
    component: page('courses/overview/index.vue'),
    children: [
      { path: '', redirect: { name: 'overview.details' } },
      { path: 'details', name: 'overview.details', component: page('courses/overview/details.vue') },
      { path: 'schedules', name: 'overview.schedules', component: page('courses/overview/schedule.vue') },
      { path: 'logs', name: 'overview.logs', component: page('courses/overview/logs.vue') },
    ] },
  { path: '/activities',
    component: page('activities/index.vue'),
    children: [
      { path: '', redirect: { name: 'activities.list' } },
      { path: 'list', name: 'activities.list', component: page('activities/list.vue') },
      { path: 'cleared', name: 'activities.cleared', component: page('activities/cleared.vue') }
    ] },
  { path: '/ranks',
    component: page('ranks/index.vue'),
    children: [
      { path: '', redirect: { name: 'ranks.list' } },
      { path: 'list', name: 'ranks.list', component: page('ranks/list.vue') },
      { path: 'create', name: 'ranks.create', component: page('ranks/create.vue') },
      { path: 'divisions', name: 'ranks.divisions', component: page('ranks/divisionlist.vue') },
      { path: 'divisions-create', name: 'ranks.createdivision', component: page('ranks/divisioncreate.vue') }
    ] },
  { path: '/categories',
    component: page('categories/index.vue'),
    children: [
      { path: '', redirect: { name: 'categories.list' } },
      { path: 'list', name: 'categories.list', component: page('categories/list.vue') },
      { path: 'create', name: 'categories.create', component: page('categories/create.vue') }
    ] },
  { path: '/exams',
    component: page('exams/index.vue'),
    children: [
      { path: '', redirect: { name: 'exams.list' } },
      { path: 'list', name: 'exams.list', component: page('exams/list.vue') },
      { path: 'create', name: 'exams.create', component: page('exams/create.vue') }
    ] },
  { path: '/settings',
    component: page('settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: page('settings/profile.vue') },
      { path: 'password', name: 'settings.password', component: page('settings/password.vue') },
      { path: 'photo', name: 'settings.photo', component: page('settings/photo.vue') }
    ] },
  { path: '*', component: page('errors/404.vue') }
]
