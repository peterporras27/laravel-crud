@php
$config['appName'] = config('app.name');
$config['locale'] = $locale = app()->getLocale();
$config['locales'] = config('app.locales');
@endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name') }}</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
    @laravelPWA
</head>
<body>
    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
        <div id="app"></div>
    </div>
    <script>
        window.config = @json($config);
    </script>
    <script src="{{ asset('js/all.js') }}"></script>
</body>
</html>
