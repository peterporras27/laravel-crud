<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        
        $roles = $this->getRoles();

        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'street' => $this->street,
            'street_no' => $this->street_no,
            'zip' => $this->zip,
            'city' => $this->city,
            'phone' => $this->phone,
            'bank_iban' => $this->bank_iban,
            'bank_bic' => $this->bank_bic,
            'image' => $this->image,
            'member_id' => $this->member_id,
            'course_id' => $this->course_id,
            'customer_id' => $this->customer_id,
            'role' => (isset($roles[0])) ? $roles[0]: 'customer',

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
