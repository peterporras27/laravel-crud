<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;

class ProfileController extends Controller
{
    /**
     * Update the user's profile information.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = $request->user();

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'street' => 'nullable|string',
            'street_no' => 'nullable|string',
            'city' => 'nullable|string',
            'zip' => 'nullable|integer',
            'phone' => 'nullable|string',
            'course_id' => 'nullable|integer',
            'customer_id' => 'nullable|integer',
        ]);

        // Remove role
        $user->roles()->detach();

        // Attach role
        $role = Role::where('slug','=',$request->role)->first();
        $user->roles()->attach($role);

        $user->permissions()->detach();
        $permissions = $role->permissions()->get();

        foreach($permissions as $perm)
        {
            $user->permissions()->attach($perm);
        }

        return tap($user)->update(
            $request->only(
                'first_name',
                'last_name',
                'email',
                'street',
                'street_no',
                'zip',
                'city',
                'phone',
                'image',
                'course_id',
                'customer_id'
            )
        );
    }
}
