<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role\UserRole;
use App\Models\ExamLog;
use App\Models\RankUp;
use App\Models\Order;
use App\Models\Rank;
use App\Models\Exam;
use App\Models\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','activity']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

        $this->params = array(
            'error' => true,
            'message' => 'Please try again.',
            'data' => null,
        );

        date_default_timezone_set(env('TIMEZONE','Europe/Berlin'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function examlogs(Request $request)
    {
        if ($this->user->can('manage-exam')) {
            $this->params['error'] = false;
            $this->params['message'] = 'Success';
            $this->params['data'] = ExamLog::orderBy('id', 'desc')->with('rank')->with('category')->with('exam')->with('user')->paginate(20);
        }
        return response()->json($this->params);
    }

    public function updateExamLogs(Request $request, $id)
    {
        if ($this->user->can('update-exam')) {

            $this->validate($request,[
                'status' => 'required|string|in:in_progress,passed,failed',
            ]);

            $log = ExamLog::findOrFail($id);
            $log->fill($request->all());
            $log->save();

            // check if all exams are passed.
            if ($request->status=='passed') 
            {
                $passed = ExamLog::where([
                    ['category_id', '=', $log->category->id],
                    ['user_id', '=', $log->user->id],
                    ['rank_id', '=', $log->user->rank_id],
                    ['status', '=', 'passed']
                ])->count();

                $required = Exam::where([
                    ['category_id', '=', $log->category->id],
                    ['course_id', '=', $log->user->course_id]
                ])->count();

                if ( $passed == $required ) 
                {
                    // Check if user has reached max points for his rank
                    $max_points = $log->rank->max_points;
                    $points = $log->user->totalPoints();

                    // check if points is maxed
                    if ($points >= $max_points) 
                    {
                        // upgrade rank
                        $this->userRankUp($log->user->id);
                        
                        $this->params['error'] = false;
                        $this->params['message'] = 'Succeess';
                    }
                }
            }
        }

        return response()->json($this->params);
    }

    public function userRankUp($user_id)
    {
        $user = User::findOrFail($user_id);

        // incriment current rank
        $new_rank = $user->rank->rank+1;

        // Previous rank
        $prev_rank = $user->rank->id;

        // find rank
        $rank = Rank::where([
            ['rank','=',$new_rank],
            ['division_id','=',$user->rank->division_id]
        ])->first();

        if ($rank) 
        {
            $user->rank_id = $rank->id;
            $user->points = 0; // reset points
            $user->start_date = date('Y-m-d H:i:s'); // reset date

            // check rank duration
            $s = ($rank->duration > 1) ? 's':'';
            $plus = strtotime("+ ".$rank->duration." ".$rank->duration_type.$s);
            $user->end_date = date("Y-m-d", $plus);
            $user->course_id = null;
            $user->save();

            // check if user ranked up previously
            $up = Order::where([
                ['user_id','=',$user->id],
                ['prev_rank_id','=',$prev_rank]
            ])->first();

            if (!$up) 
            {
                /*
                $rankup = new RankUp();
                $rankup->user_id = $user->id;
                $rankup->prev_rank_id = $prev_rank;
                $rankup->rank_id = $rank->id;
                $rankup->save();
                */

                // TODO: Send request for new belt
                $order = new Order();
                $order->belt = $rank->name;
                $order->user_id = $user->id;
                $order->prev_rank_id = $prev_rank;
                $order->rank_id = $rank->id;
                $order->save();
            }
        }
    }

    public function getCustomerTrainees(Request $request, $id)
    {
        $this->params['data'] = User::where('customer_id','=',$id)->paginate(10);

        return response()->json($this->params);
    }

    public function getOrders()
    {
        $this->params['data'] = Order::orderBy('id', 'desc')->with('prev')
            ->with('next')
            ->with('user')
            ->paginate(10);

        return response()->json($this->params);
    }

    public function updateOrder(Request $request, $id)
    {
        if ($this->user->can('update-exam')) {

            $this->validate($request,[
                'status' => 'required|string|in:hold,processing,success',
            ]);

            $order = Order::findOrFail($id);
            $order->fill($request->all());
            $order->save();

            $this->params['error'] = false;
            $this->params['message'] = 'Success';
        }

        return response()->json($this->params);
    }
}
