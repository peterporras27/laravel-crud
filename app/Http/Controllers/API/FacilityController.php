<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Facility;
use App\Models\City;
use Auth;

class FacilityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','activity']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

        $this->params = array(
            'title' => 'Facilities',
            'cities' => City::all()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($this->user->can('manage-facility')) {

            $perpage = 10;

            if ( $request->input('perpage') ) {
                $perpage = preg_replace('/\D/', '', $request->input('perpage'));
                $perpage = !empty($perpage) ? $perpage : 50;   
            }

            $this->params['data'] = Facility::orderBy('id', 'asc')->with('city')->paginate($perpage);
            $this->params['perpage'] = $perpage;
        }

        return response()->json($this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->user->can('create-facility')) {

            $this->validate($request, [
                'name' => 'required|string',
                'city_id' => 'required|integer'
            ]);

            $facility = new Facility();
            $facility->fill($request->all());
            $facility->save();
        }

        return response()->json($this->params);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->user->can('update-facility')) {

            $facility = Facility::find($id);

            $this->validate($request, [
                'name' => 'required|string',
                'city_id' => 'required|integer'
            ]);

            $facility->fill($request->all());
            $facility->save();
        }

        return response()->json($this->params);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-facility')) {

            $facility = Facility::find($id);

            if ($facility) {
                $facility->delete();    
            }
        }

        return response()->json($this->params);
    }
}
