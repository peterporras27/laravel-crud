<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Facility;
use App\Models\Course;
use App\Models\User;
use App\Models\Role;
use Auth;

class CourseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','activity']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

        $this->params = array(
            'title' => 'Available Courses',
            'facilities' => Facility::all()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($this->user->can('manage-course')) 
        {

            $perpage = 10;
            $courses = Course::orderBy('id', 'asc');
            if ( $request->input('perpage') ) {
                $perpage = preg_replace('/\D/', '', $request->input('perpage'));
                $perpage = !empty($perpage) ? $perpage : 50;   
            }

            if ($this->user->hasRole('employee')) {
                $courses->where('employee_id','=',$this->user->id);
            }

            $courses->with('facility')
                ->with('rank')
                ->with('users');

            $this->params['data'] = $courses->paginate($perpage);
            $this->params['perpage'] = $perpage;
        }

        return response()->json($this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if ($this->user->can('create-course')) 
        {

            $this->validate($request, [
                'title' => 'required|string',
                'facility_id' => 'required|integer',
                'employees' => 'nullable|array',
                'rank_id' => 'required|integer'
            ]);

            $course = new Course();
            $course->fill($request->all());
            $course->save();

            $employees = User::whereIn('id',$request->input('employees'))->get();

            if ($employees) 
            {
                foreach ($employees as $employee) 
                {
                    $employee->courses()->attach($course);
                }
            }
        }

        return response()->json($this->params);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::find($id);

        $this->params['data'] = $course;
        $this->params['rank'] = $course->rank;
        $this->params['instructor'] = ($course->employee) ? $course->employee->first_name.' '.$course->employee->last_name:'';
        $this->params['facility'] = ($course->facility) ? $course->facility->name:'';
        $this->params['city'] = ($course->facility) ? $course->facility->city->name:'';

        return response()->json($this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->user->can('update-course')) {

            $course = Course::find($id);
            
            $this->validate($request, [
                'title' => 'required|string',
                'facility_id' => 'required|integer',
                'employees' => 'nullable|array',
                'rank_id' => 'required|integer'
            ]);

            $course->fill($request->all());
            $course->save();

            $employees = User::whereIn('id',$request->input('employees'))->get();
            if ($employees) 
            {
                $course->users()->detach();
                foreach ($employees as $employee) 
                {
                    $course->users()->attach($employee);
                }    
            }
        }

        return response()->json($this->params);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-course')) 
        {
            $course = Course::find($id);
            
            if ($course) 
            {
                $course->users()->detach();
                $course->delete();
            }
        }

        return response()->json($this->params);
    }

    public function overview(Request $request, $id)
    {
        $course = Course::find($id);

        $this->params['title'] = $course->title;
        $this->params['course'] = $course;

        $perpage = 10; $search = '';
        $users = User::orderBy('id', 'asc')
            ->where([
                ['course_id','=',$course->id],
                ['rank_id','=',$course->rank_id],
            ]);

        if ( $request->input('perpage') ) {
            $perpage = preg_replace('/\D/', '', $request->input('perpage'));
            $perpage = !empty($perpage) ? $perpage : 50;   
        }

        $users->whereHas('roles', function($q) {
            $q->where('slug', 'trainee');
        });

        if ($request->input('s')) {
            $search = $request->input('s');
            $users->where('first_name', 'LIKE', "%{$search}%") 
                ->orWhere('last_name', 'LIKE', "%{$search}%")
                ->orWhere('email', 'LIKE', "%{$search}%");
        }

        $this->params['data'] = $users->paginate($perpage);
        $this->params['perpage'] = $perpage;
        $this->params['search'] = $search;

        return response()->json($this->params);   
    }
}
