<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Auth;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','activity']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

        $this->params = array(
            'error' => true,
            'message' => 'Please try again.',
            'title' => 'Categories',
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($this->user->can('manage-category')) {

            $perpage = 10;

            if ( $request->input('perpage') ) {
                $perpage = preg_replace('/\D/', '', $request->input('perpage'));
                $perpage = !empty($perpage) ? $perpage : 50;   
            }

            $category = Category::orderBy('id', 'asc');

            if ( $request->input('division') ) {
                $category->where('division_id','=',$request->input('division'));
            }

            $this->params['data'] = $category->with('division')->paginate($perpage);
            $this->params['perpage'] = $perpage;
            $this->params['error'] = false;
            $this->params['message'] = 'success';
        }

        return response()->json($this->params); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->user->can('create-category')) 
        {
            $this->validate($request, [
                'name' => 'required|string',
                'min_points' => 'required|integer',
                'division_id' => 'required|integer'
            ]);

            $category = new Category();
            $category->fill($request->all());
            $category->save();

            $this->params['error'] = false;
            $this->params['message'] = 'success';
        } 

        return response()->json($this->params); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->user->can('update-category')) 
        {
            $category = Category::find($id);

            $this->validate($request, [
                'name' => 'required|string',
                'min_points' => 'required|integer',
                'division_id' => 'required|integer'
            ]);

            $category->fill($request->all());
            $category->save();

            $this->params['error'] = false;
            $this->params['message'] = 'success';

        }

        return response()->json($this->params); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-category')) 
        {
            $category = Category::find( $id );

            if ( $category )
            {
                $category->delete();
                $this->params['error'] = false;
                $this->params['message'] = 'success';
            }
        }

        return response()->json($this->params); 
    }
}
