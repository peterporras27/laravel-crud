<?php

namespace App\Http\Controllers\API;

use jeremykenedy\LaravelLogger\App\Http\Traits\ActivityLogger;
use App\Http\Resources\UserCollection;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Imports\UsersImport;

use App\Models\Permission;
use App\Models\ExamLog;
use App\Models\Course;
use App\Models\Rank;
use App\Models\User;
use App\Models\Role;
use App\Models\Exam;
use Image;
use Auth;
use Str;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','activity']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

        $this->params = array(
            'error' => true,
            'message' => 'Please try again',
            'title' => 'Users',
            'data' => null,
            'roles' => Role::all()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($this->user->can('manage-user')) {

            $perpage = 10; $search = '';
            $users = User::orderBy('id', 'asc');

            if ( $request->perpage ) {
                $perpage = preg_replace('/\D/', '', $request->perpage);
                $perpage = !empty($perpage) ? $perpage : 50;   
            }

            if ($request->role) 
            {
                $users->whereHas('roles', function($q) use ($request) {
                    $q->where('slug', $request->role);
                });
            }

            if ($request->search) {
                $search = $request->search;
                $users->where('first_name', 'LIKE', "%{$search}%") 
                    ->orWhere('last_name', 'LIKE', "%{$search}%")
                    ->orWhere('email', 'LIKE', "%{$search}%");
            }

            $this->params['data'] = $users->with('roles')->with('rank')->paginate($perpage);
            $this->params['role'] = $request->role;
            $this->params['perpage'] = $perpage;
            $this->params['request'] = $request;
            $this->params['search'] = $search;
            $this->params['error'] = false;
            $this->params['message'] = 'Success';

        } 

        return response()->json($this->params); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( $this->user->can('create-user') )
        {
            $this->validate($request, [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:8|confirmed',
                
                'street' => 'nullable|string',
                'street_no' => 'nullable|string',
                'city' => 'nullable|string',
                'zip' => 'nullable|integer',
                'phone' => 'nullable|string',
                'course_id' => 'nullable|integer',
                'rank_id' => 'nullable|integer',
            ]);

            $user = new User();
            $user->fill( $request->all() );
            $user->password = app('hash')->make($request->input('password'));
            $user->save();
            
            $slug = 'trainee';
            $userRoles = ['admin','employee','customer','trainee'];

            if ( $request->role ) 
            {
                if ( in_array($request->role, $userRoles) ) {
                    $slug = $request->role;
                } else {
                    $slug = 'customer';
                }
            }

            // assign role
            $role = Role::where('slug','=',$slug)->first();
            if ($role) 
            {
                $user->roles()->attach( $role );
                $user->permissions()->detach();
                $permissions = $role->permissions()->get();

                foreach( $permissions as $perm )
                {
                    $user->permissions()->attach($perm);
                }
            }

            /* if ($slug=='trainee') 
            {
                $rank = Rank::where('rank','=',0)->first();

                if ($rank)
                {
                    // Set start date
                    $user->start_date = date('Y-m-d H:i:s');
                    // check rank duration
                    $s = ($rank->duration > 1) ? 's':'';
                    $plus = strtotime("+ ".$rank->duration." ".$rank->duration_type.$s);
                    $user->end_date = date("Y-m-d", $plus);
                    $user->save();
                }
            } */

            $this->params['error'] = false;
            $this->params['message'] = 'Success';
        }

        return response()->json($this->params); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if ( $user ) 
        {
            $roles = $user->getRoles();
            $user->role = (isset($roles[0])) ? $roles[0]:'trainee';
            $user->photo_url = 'https://www.gravatar.com/avatar/'.md5(strtolower($user->email)).'.jpg?s=200&d=mm';
            $this->params['course'] = $user->course;
            $this->params['error'] = false;
            $this->params['message'] = 'Success';
            $this->params['data'] = $user;
            $this->params['points'] = $user->totalPoints();
            $this->params['rank'] = $user->rank;
            $this->params['traininglogs'] = $user->trainingLogs()->paginate(10);
            $this->params['examlogs'] = $user->examLogs()->paginate(10);
            $this->params['examlist'] = Exam::where([
                ['rank_id','=',$user->rank_id],
                ['course_id','=',$user->course_id]
            ])->paginate(10);
        }

        return response()->json($this->params); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($this->user->can('update-user')) 
        {
            $user = User::find($request->id);

            if ($user) 
            {
                $validate = [
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'street' => 'nullable|string',
                    'street_no' => 'nullable|string',
                    'city' => 'nullable|string',
                    'zip' => 'nullable|integer',
                    'phone' => 'nullable|string',
                    'course_id' => 'nullable|integer',
                    'rank_id' => 'nullable|integer'
                ];

                if ($user->email != $request->email) 
                {
                    $validate['email'] = 'required|string|email|max:255|unique:users';
                }
                
                $this->validate($request, $validate);

                $user->fill($request->all());
                $user->save();

                // Remove role
                $user->roles()->detach();

                // Attach role
                $role = Role::where('slug','=',$request->role)->first();
                $user->roles()->attach($role);

                $user->permissions()->detach();
                $permissions = $role->permissions()->get();

                foreach($permissions as $perm)
                {
                    $user->permissions()->attach($perm);
                }

                if ($request->rank_id) 
                {
                    $rank = Rank::find($request->rank_id);

                    if ($rank) 
                    {
                        // Set start date
                        $user->start_date = date('Y-m-d H:i:s');
                        // check rank duration
                        $s = ($rank->duration > 1) ? 's':'';
                        
                        $plus = strtotime("+ ".$rank->duration." ".$rank->duration_type.$s);
                        $user->end_date = date("Y-m-d", $plus);
                        $user->save();
                    }
                }

                $this->params['error'] = false;
                $this->params['message'] = 'Success';
            }

        } 

        return response()->json($this->params); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-user')) 
        {
            $user = User::find($id);

            if ( $user ) 
            {
                $this->params['error'] = false;
                $this->params['message'] = 'Success';
                $user->delete();
            }
        }

        return response()->json($this->params); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function csvupload(Request $request)
    {
        if ($this->user->can('manage-user')) 
        {

            $file = $request->file('csv');

            // File Details 
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            // Valid File Extensions
            $valid_extension = array("csv");

            // Check file extension
            if(in_array(strtolower($extension),$valid_extension))
            {
                Excel::import(new UsersImport, $file);
                $this->params['error'] = false;
                $this->params['message'] = 'Success';
            }
        } 

        return response()->json($this->params); 
    }

    public function assignTrainees(Request $request, $id)
    {
        $user = User::find($id);

        $this->validate($request, [
            'trainees' => 'required|array'
        ]);

        $children = User::where('customer_id','=',$id)->get();
        foreach($children as $child)
        {
            $child->customer_id = null;
            $child->save();
        }
        
        foreach ($request->input('trainees') as $uid) 
        {
            $u = User::find($uid);

            if ($u) 
            {
                $u->customer_id = $id;
                $u->save();   
            }
        }

        $this->params['error'] = false;
        $this->params['message'] = 'Success';
        
        return response()->json($this->params); 
    }

    public function imageUpload(Request $request)
    {
        $this->validate($request,[
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:10048'
        ]);

        if ($request->hasFile('image') && $request->file('image')->isValid() ) 
        {
            if (!empty($this->user->image)) 
            {
                if ( file_exists( public_path('/uploads/'.$this->user->image) ) ) 
                {
                    unlink(public_path('/uploads/'.$this->user->image));
                }
            }

            $filename  = (string) Str::uuid() . '.' . $request->image->extension();
            $imgpath   = '/uploads/' . $filename;

            Image::make( $request->image )->save( public_path($imgpath) );

            $this->user->image = $filename;
            $this->user->save();

            $this->params['image'] = $filename;
        }

        return response()->json($this->params); 
    }

    public function profileImgUpload(Request $request, $id)
    {
        $this->validate($request,[
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:10048'
        ]);

        $user = User::findOrFail($id);

        if ($request->hasFile('image') && $request->file('image')->isValid() ) 
        {
            if (!empty($user->image)) 
            {
                if ( file_exists( public_path('/uploads/'.$user->image) ) ) 
                {
                    unlink(public_path('/uploads/'.$user->image));
                }
            }

            $filename  = (string) Str::uuid() . '.' . $request->image->extension();
            $imgpath   = '/uploads/' . $filename;

            Image::make( $request->image )->save( public_path($imgpath) );

            $user->image = $filename;
            $user->save();

            $this->params['image'] = $filename;
        }

        return response()->json($this->params); 
    }

    public function getTrainees(Request $request, $id)
    {
        if ($this->user->can('edit-user')) 
        {
            $perpage = 10; $search = '';
            $user = User::findOrFail($id);
            $trainees = User::whereHas('roles', function($q) {
                    $q->where('slug', 'trainee');
                })
                ->whereNull('customer_id')
                ->orWhere('customer_id','=',$id);

            if ( $request->input('perpage') ) {
                $perpage = preg_replace('/\D/', '', $request->input('perpage'));
                $perpage = !empty($perpage) ? $perpage : 10;   
            }

            if ($request->input('s')) {
                $search = $request->input('s');
                $trainees->where('first_name', 'LIKE', "%{$search}%") 
                    ->orWhere('last_name', 'LIKE', "%{$search}%");
            }

            $children = User::where('customer_id','=',$id)->pluck('id')->toArray();

            $this->params['perpage'] = $perpage;
            $this->params['search'] = $search;
            $this->params['data'] = $trainees->paginate($perpage);
            $this->params['children'] = (is_array($children)) ? $children:[];

            $this->params['error'] = false;
            $this->params['message'] = 'success';
        }

        return response()->json($this->params); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function takethetest(Request $request)
    {
        $this->validate($request, [
            'exam_id' => 'required|integer',
            'user_id' => 'required|integer'
        ]);

        $exam = Exam::findOrFail($request->exam_id);
        $user = User::findOrFail($request->user_id);

        $this->params['examlogs'] = [];

        // check if user has already taken the exam
        $examlog = ExamLog::where([
            ['exam_id','=',$request->exam_id],
            ['user_id','=',$request->user_id],
        ])->first();

        // Check if user has enough required points to take the test
        if( $user->totalPoints() < $exam->category->min_points ){
            $this->params['message'] = "Trainee is required to have at least ".$exam->category->min_points." points to take this test.";
            return response()->json($this->params);
        }

        // check if user is allowed to take this exam.
        if ($user->rank_id == $exam->rank_id && $exam->course_id == $user->course_id) 
        {
            if ($examlog) {
                
                $this->params['message'] = 'Exam has already been taken or undergoing.';

            } else {

                $log = new ExamLog();
                $log->name = $exam->name;
                $log->rank_id = $user->rank_id;
                $log->category_id = $exam->category_id;
                $log->user_id = $user->id;
                $log->exam_id = $exam->id;
                $log->save();

                $this->params['error'] = false;
                $this->params['message'] = 'Request for exam was sent.';
                $this->params['examlogs'] = ExamLog::where([
                    ['exam_id','=',$request->exam_id],
                    ['user_id','=',$request->user_id],
                ])->with('category')->paginate(10);
            }

        } else {

            $this->params['message'] = 'You are not allowed to take this test at the moment.';
        }

        return response()->json($this->params);

    }
}
