<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Exam;
use Auth;

class ExamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','activity']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

        $this->params = array(
            'error' => true,
            'message' => 'Please try again.',
            'title' => 'Exams',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($this->user->can('manage-exam')) 
        {
            $perpage = 10;

            if ( $request->input('perpage') ) 
            {
                $perpage = preg_replace('/\D/', '', $request->input('perpage'));
                $perpage = !empty($perpage) ? $perpage : 50;   
            }

            $exams = Exam::orderBy('id', 'asc')
                ->with('rank')
                ->with('course')
                ->with('category')
                ->paginate($perpage);

            $this->params['data'] = $exams;
            $this->params['perpage'] = $perpage;
            $this->params['error'] = false;
            $this->params['message'] = 'success';
        }

        return response()->json($this->params); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->user->can('create-exam')) 
        {
            $this->validate($request, [
                'name' => 'required|string',
                'requirements' => 'nullable|array',
                'rank_id' => 'required|integer',
                'category_id' => 'required|integer'
            ]);

            $course = Course::where('rank_id','=',$request->input('rank_id'))->first();

            $exam = new Exam();
            $exam->fill($request->all());
            $exam->course_id = $course->id;
            $exam->save();

            $this->params['error'] = false;
            $this->params['message'] = 'success';
        } 

        return response()->json($this->params); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->user->can('update-exam')) 
        {
            $exam = Exam::find($id);

            $this->validate($request, [
                'name' => 'required|string',
                'requirements' => 'nullable|array',
                'rank_id' => 'required|integer',
                'category_id' => 'required|integer'
            ]);

            $course = Course::where('rank_id','=',$request->input('rank_id'))->first();

            $exam->fill($request->all());
            $exam->course_id = $course->id;
            $exam->save();

            $this->params['error'] = false;
            $this->params['message'] = 'success';

        }

        return response()->json($this->params);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-exam')) 
        {
            $exam = Exam::find( $id );

            if ( $exam )
            {
                $exam->delete();
                $this->params['error'] = false;
                $this->params['message'] = 'success';
            }
        }

        return response()->json($this->params); 
    }
}
