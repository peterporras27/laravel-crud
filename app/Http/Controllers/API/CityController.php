<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\User;
use App\Models\Role;
use Auth;

class CityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','activity']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

        $this->params = array(
            'error' => true,
            'message' => 'Please try again.',
            'title' => 'Cities',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($this->user->can('manage-city')) {

            $perpage = 10;

            if ( $request->input('perpage') ) {
                $perpage = preg_replace('/\D/', '', $request->input('perpage'));
                $perpage = !empty($perpage) ? $perpage : 50;   
            }

            $this->params['data'] = City::orderBy('id', 'asc')->paginate($perpage);
            $this->params['perpage'] = $perpage;
            $this->params['error'] = false;
            $this->params['message'] = 'success';
        }

        return response()->json($this->params); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->user->can('create-city')) 
        {
            $this->validate($request, [
                'name' => 'required|string',
                'zip_code' => 'required|integer'
            ]);

            $city = new City();
            $city->fill($request->all());
            $city->save();

            $this->params['error'] = false;
            $this->params['message'] = 'success';
        } 

        return response()->json($this->params); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->user->can('update-city')) 
        {
            $city = City::find($id);

            $this->validate($request, [
                'name' => 'required|string',
                'zip_code' => 'required|integer'
            ]);

            $city->fill($request->all());
            $city->save();

            $this->params['error'] = false;
            $this->params['message'] = 'success';

        }

        return response()->json($this->params); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-city')) 
        {
            $city = City::find( $id );

            if ( $city )
            {
                $city->delete();
                $this->params['error'] = false;
                $this->params['message'] = 'success';
            }
        }

        return response()->json($this->params); 
    }
}
