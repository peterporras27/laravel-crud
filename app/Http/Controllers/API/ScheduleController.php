<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TrainingHour;
use Auth;

class ScheduleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','activity']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

        $this->params = array(
            'error' => true,
            'message' => 'Please try again.',
            'title' => 'Schedules',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($this->user->can('manage-schedule')) 
        {
            $perpage = 10;

            if ( $request->input('perpage') ) {
                $perpage = preg_replace('/\D/', '', $request->input('perpage'));
                $perpage = !empty($perpage) ? $perpage : 50;   
            }

            $this->params['data'] = TrainingHour::orderBy('id', 'asc')->paginate($perpage);
            $this->params['perpage'] = $perpage;
            $this->params['error'] = false;
            $this->params['message'] = 'success';
        }

        return response()->json($this->params); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->user->can('create-schedule')) 
        {
            $this->validate($request, [
                'day' => 'required|string', 
                'start_time' => 'required|string', 
                'end_time' => 'required|string', 
                'course_id' => 'required|integer'
            ]);

            $sched = new TrainingHour();
            $sched->fill($request->all());
            $sched->save();

            $this->params['error'] = false;
            $this->params['message'] = 'success';
        } 

        return response()->json($this->params); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return TrainingHour::where('course_id','=',$id)->paginate(10);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->user->can('update-schedule')) 
        {
            $sched = TrainingHour::find($id);

            $this->validate($request, [
                'day' => 'required|string', 
                'start_time' => 'required|string', 
                'end_time' => 'required|string', 
                'course_id' => 'required|integer', 
            ]);

            $sched->fill($request->all());
            $sched->save();

            $this->params['error'] = false;
            $this->params['message'] = 'success';
        }

        return response()->json($this->params); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-schedule')) 
        {
            $sched = TrainingHour::find( $id );

            if ( $sched )
            {
                $sched->delete();
                $this->params['error'] = false;
                $this->params['message'] = 'success';
            }
        }

        return response()->json($this->params); 
    }
}
