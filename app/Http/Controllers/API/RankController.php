<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rank;
use Auth;

class RankController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','activity']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

        $this->params = array(
            'title' => 'Ranks'
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($this->user->can('manage-rank')) 
        {
            $perpage = 10;
            $rank = Rank::orderBy('id', 'asc');

            if ( $request->input('perpage') ) 
            {
                $perpage = preg_replace('/\D/', '', $request->input('perpage'));
                $perpage = !empty($perpage) ? $perpage : 50;   
            }

            $this->params['data'] = $rank->with('division')->paginate($perpage);
            $this->params['perpage'] = $perpage;
        }

        return response()->json($this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->user->can('create-rank')) 
        {
            $this->validate($request, [
                'name' => 'required|string',
                'rank' => 'required|integer',
                'color' => 'nullable|string',
                'duration' => 'required|integer',
                'duration_type' => 'required|string',
                'max_points' => 'required|integer',
            ]);

            $rank = new Rank();
            $rank->fill($request->all());
            $rank->save();

            $this->params['error'] = false;
            $this->params['message'] = 'success';
        } 

        return response()->json($this->params); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->user->can('update-rank')) 
        {
            $rank = Rank::find($id);

            $this->validate($request, [
                'name' => 'required|string',
                'rank' => 'required|integer',
                'color' => 'nullable|string',
                'duration' => 'required|integer',
                'duration_type' => 'required|string',
                'max_points' => 'required|integer',
            ]);

            $rank->fill($request->all());
            $rank->save();

            $this->params['error'] = false;
            $this->params['message'] = 'success';

        }

        return response()->json($this->params); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->can('delete-rank')) 
        {
            $rank = Rank::find( $id );

            if ( $rank )
            {
                $rank->delete();
                $this->params['error'] = false;
                $this->params['message'] = 'success';
            }
        }

        return response()->json($this->params); 
    }
}
