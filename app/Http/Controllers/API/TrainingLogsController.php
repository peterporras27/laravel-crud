<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TrainingLog;
use Auth;

class TrainingLogsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','activity']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

        $this->params = array(
            'error' => true,
            'message' => 'Please try again.',
            'title' => 'Training Logs',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($this->user->can('manage-schedule')) {

            $perpage = 10;

            if ( $request->input('perpage') ) {
                $perpage = preg_replace('/\D/', '', $request->input('perpage'));
                $perpage = !empty($perpage) ? $perpage : 20;   
            }

            $logs = TrainingLog::orderBy('id', 'asc');

            if ( $request->input('course_id') ) {
                $logs->where('course_id','=',$request->input('course_id'));
            }

            $this->params['data'] = $logs->with('trainee')->paginate($perpage);
            $this->params['perpage'] = $perpage;
            $this->params['error'] = false;
            $this->params['message'] = 'success';
        }

        return response()->json($this->params); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->user->can('update-schedule')) 
        {
            $log = TrainingLog::find($id);

            $this->validate($request, [
                'points' => 'required|integer',
            ]);

            $log->fill($request->all());
            $log->save();

            $this->params['error'] = false;
            $this->params['message'] = 'success';
        }

        return response()->json($this->params); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
