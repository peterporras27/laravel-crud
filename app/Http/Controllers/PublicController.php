<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Facility;
use App\Models\Category;
use App\Models\Division;
use App\Models\Course;
use App\Models\Rank;
use App\Models\User;
use App\Models\City;
use App\Models\Role;
use Response;
use Image;
use Auth;

class PublicController extends Controller
{
    public function getRoles()
    {
        return Role::all();
    }

    public function getCities()
    {
        return City::all();
    }

    public function getFacilities(Request $request)
    {
        if ($request->city_id) {
            $facility = Facility::where('city_id','=',$request->input('city_id'))->get();
        } else {
            $facility = Facility::all();
        }

        return $facility;
    }

    public function getRanks()
    {
        return Rank::with('division')->get();
    }

    public function getCourses(Request $request)
    {
        if ($request->rank_id) {
            $course = Course::where('rank_id','=',$request->input('rank_id'))->get();
        } else {
            $course = Course::all();
        }

        return $course;
    }

    public function getCategories(Request $request)
    {
        $category = Category::orderBy('id', 'asc');

        if ( $request->input('division') ) {
            $category->where('division_id','=',$request->input('division'));
        }

        return $category->get();
    }

    public function getDivisions()
    {
        return Division::all();
    }

    public function getEmployees()
    {
        return User::whereHas('roles', function($q) {
                $q->where('slug', 'employee');
            })->get();
    }

    public function image( $size = 'lg', $imgurl )
    {

        $imgurl = public_path('/uploads/'.$imgurl);

        if (file_exists($imgurl)) 
        {
            switch ($size) {
                case 'xx':
                    $image = Image::make( $imgurl )->resize(50, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->fit(50)->response();
                    break;
            	case 'xs':
            		$image = Image::make( $imgurl )->resize(100, null, function ($constraint) {
    				    $constraint->aspectRatio();
    				})->fit(100)->response();
            		break;
            	case 'sm':
            		$image = Image::make( $imgurl )->resize(300, null, function ($constraint) {
    				    $constraint->aspectRatio();
    				})->fit(300)->response();
            		break;
            	case 'lg':
            		$image = Image::make( $imgurl )->response();
            		break;
                case 'xxl':
                    $image = Image::make( $imgurl )->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->fit(800)->response();
                    break;
            }

        } else {

            $image = Image::make( asset('hdlogo.png') )->response();
        }

        return $image;
    }
}
