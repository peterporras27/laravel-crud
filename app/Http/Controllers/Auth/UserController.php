<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Get authenticated user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function current(Request $request)
    {
        $user = $request->user();
        $roles = $user->getRoles();
        $user->role = (isset($roles[0])) ? $roles[0]:'customer';
        $user->permissions = $user->getPermissions();
        $user->photo_url = $user->image ? '/img/lg/'.$user->image:'/hdlogo.png';
        return response()->json($user);
    }
}
