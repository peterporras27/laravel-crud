<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;
use Image;
use Str;

class SettingsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','activity']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

        $this->params = array(
            'title' => 'User Settings'
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->params['user'] = $this->user;

        return view('settings.index', $this->params);  

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'street' => 'nullable|string',
            'street_no' => 'nullable|string',
            'city' => 'nullable|string',
            'zip' => 'nullable|integer',
            'phone' => 'nullable|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ];

        if ($this->user->email != $request->input('email')) 
        {
            $validate['email'] = 'required|string|email|max:255|unique:users';
        }

        if ($request->input('password')) 
        {
            $validate['password'] = 'required|string|min:8|confirmed';
        }

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) 
        {
            return redirect('settings')
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('image') && $request->file('image')->isValid() ) 
        {
            if (!empty($this->user->image)) 
            {
                if ( file_exists( public_path('/uploads/'.$this->user->image) ) ) 
                {
                    unlink(public_path('/uploads/'.$this->user->image));
                }
            }

            $filename  = (string) Str::uuid() . '.' . $request->image->extension();
            $imgpath   = '/uploads/' . $filename;

            Image::make( $request->image )->save( public_path($imgpath) );

            $this->user->image = $filename;
        }

        $this->user->first_name = $request->input('first_name');
        $this->user->last_name = $request->input('last_name');
        $this->user->street = $request->input('street');
        $this->user->street_no = $request->input('street_no');
        $this->user->city = $request->input('city');
        $this->user->zip = $request->input('zip');
        $this->user->phone = $request->input('phone');

        if ($request->input('password')) 
        {
            $this->user->password = app('hash')->make($request->input('password'));
        }
        
        $this->user->save();

        return redirect('settings')->with('success', __('Profile successfully updated'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
