<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\TrainingLog;
use App\Models\Course;
use App\Models\ExamLog;
use App\Models\Exam;
use App\Models\User;
use App\Models\Role;
use DateTime;

class ApiController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->params = array(
            'error' => true,
            'message' => 'Please try again',
            'data' => null
        );

        $this->timezone = config('app.timezone', 'Europe/Berlin');

        $this->apiKey = '5712bde843b95864ca8d219729ad52e4';
    }

    public function pronomous(Request $request)
    {
        if ( $request->input('key') == $this->apiKey )
        {
            $data = $this->mappedData($request->all());
            $member_number = isset($data['membership_number']) ? $data['membership_number']: null;
            $isnew = true;

            // Check if user update
            if ($member_number) 
            {
                $user = User::where('membership_number','=',$member_number)->first();

                if ($user) 
                {
                    $user->fill($data);
                    $user->save();
                    $this->params['message'] = 'Member updated successfully';

                    $isnew = false;
                }
            }

            if ($isnew) 
            {
                $role = Role::where('slug','customer')->first();

                $user = new User();
                $user->fill($data);
                $user->password = Hash::make(md5($member_number));
                $user->save();
                $user->roles()->attach($role);

                $this->params['message'] = 'Member registered successfully';
            }

            $this->params['error'] = false;
        }

        return response()->json($this->params);
    }

    private function mappedData($data)
    {
        $returned = [];
        if (count($data)) {
            $columns = array(
                'membership_number' => "Mitglie",
                'status' => "Status",
                'salutation' => "Anrede",
                'first_name' => "Vorname",
                'last_name' => "Nachname",
                'zip_code' => "PLZ",
                'residence' => "Wohnort",
                'address' => "Adresse",
                'phone' => "Telefon",
                'email' => "E-Mail",
                'birthday' => "Geburtstag",
                'age' => "Alter",
                'initial_contract' => "Erstvertrag",
                'actual_contract' => "Atk. Vertrag",
                'duration' => "Dauer",
                'end' => "Ende",
                'payment_method' => "Zahlungsart",
                'contribution' => "Beitrag",
                'uid' => "UID",
                'booking_day' => "Buchungstag",
                'admission' => "Aufnahme",
                'cycle' => "Zyklus",
                'termination_date' => "digungsdatum",
                'iban' => "IBAN",
            );

            foreach ($columns as $key => $value) {
                $val = $this->mapColumn($value, $data);
                if (!is_null($val)) {
                    $returned[$key] = $val; 
                }
            }
        }

        return $returned;
    }

    private function mapColumn($search, $arr)
    {
        $arr = array_change_key_case($arr, CASE_LOWER);
        $found = $this->getvalue($search,array_keys($arr));
        $key = ( count($found) ) ? array_values($found): null;
        
        if ( !is_null( $key ) ) { 
            return $arr[$key[0]]; 
        }

        return null;
    }

    private function getvalue($text, $arr)
    {
        $text = strtolower($text); // lowercase text   
        $input = preg_quote($text, '~'); // don't forget to quote input string!
        return preg_grep('~' . $input . '~', $arr);
    }

    public function log(Request $request, $hash)
    {
        // decode hash
        // base64_encode('m.bouchuari@live.de'); // bS5ib3VjaHVhcmlAbGl2ZS5kZQ==
        // base64_encode('daniela.schlebusch@googlemail.com'); ZGFuaWVsYS5zY2hsZWJ1c2NoQGdvb2dsZW1haWwuY29t
        $email = base64_decode($hash);

        // check if user exist
        $user = User::where('email','=',$email)->first();

        if ( !$user ) 
        {
            $this->params['message'] = 'You are not enrolled to any course.';
            return response()->json($this->params); 
        }

        date_default_timezone_set($this->timezone);

        // Check current day
        $current_day = strtolower(date('l'));
        $now = date('H:i:s');
        $onSchedule = false;
        $scheds = [];
        $todaySchedule = null;

        // get all schedules within the day
        foreach ($user->course->schedules as $schedule) 
        {
            if ($schedule->day==$current_day) 
            {
                $onSchedule = $this->timeIsBetweenSched($schedule->start_time, $schedule->end_time, $now);
                if ( $onSchedule ) { $todaySchedule = $schedule; }
                $scheds[] = $schedule;
            }
        }
        
        $this->params['data'] = $scheds;
        $this->params['onschedule'] = $onSchedule;
        $this->params['now'] = date('H:i:s e');
        
        // Check if time co-inside with the course schedule
        if ( $onSchedule ) 
        {
            // Check if logged on the same day
            $userlog = TrainingLog::where([
                ['course_id','=',$user->course->id],
                ['user_id','=',$user->id]
            ])->whereDate('created_at', '=', date('Y-m-d'))->get();

            $hasLog = false;

            if ( $userlog->count() > 0 )
            {
                foreach ($userlog as $ulog)
                {
                    if ( !is_null( $todaySchedule ) )
                    {
                        $login_time = strtotime($ulog->login_time);
                        $logtime = date('H:i:s', $login_time);
                        if($this->timeIsBetweenSched($todaySchedule->start_time, $todaySchedule->end_time, $logtime)){
                            $hasLog = true;
                        } 
                    }
                }
            } 

            if (!$hasLog) 
            {
                // record user log
                $log = new TrainingLog();
                $log->login_time = date('F j, Y h:i:s A');
                $log->course_id = $user->course->id;
                $log->user_id = $user->id;
                $log->save();

                $this->params['error'] = false;
                $this->params['message'] = 'Attendance successfully recorded.';

            } else {

                $this->params['error'] = false;
                $this->params['message'] = 'Attendance already recorded.';
            }

        } else {

            $this->params['message'] = 'No schedule available at this time.';
        }

        return response()->json($this->params);
    }

    public function timeIsBetweenSched($from, $till, $now)
    {
        date_default_timezone_set($this->timezone);

        $f = DateTime::createFromFormat('H:i:s', $from);
        $t = DateTime::createFromFormat('H:i:s', $till);
        $i = DateTime::createFromFormat('H:i:s', $now);
        if ($f > $t) $t->modify('+1 day');
        return ($f <= $i && $i <= $t) || ($f <= $i->modify('+1 day') && $i <= $t);
    }
}
