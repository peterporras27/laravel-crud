<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamLog extends Model
{
    protected $fillable = [
        'name', 
        'gained_points', 
        'status', 
        'rank_id', 
        'category_id', 
        'user_id', 
        'exam_id', 
    ];

    public function rank()
    {
    	return $this->hasOne(Rank::class,'id','rank_id');
    }

    public function category()
    {
    	return $this->hasOne(Category::class,'id','category_id');
    }

    public function exam()
    {
    	return $this->hasOne(Exam::class,'id','exam_id');
    }

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}
