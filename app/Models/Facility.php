<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'city_id'
    ];

    public function city(){
    	return $this->hasOne(City::class,'id','city_id');
    }
}
