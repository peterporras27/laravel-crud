<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingHour extends Model
{
    protected $fillable = [
        'day', 
        'start_time', 
        'end_time', 
        'course_id', 
    ];
}
