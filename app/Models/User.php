<?php

namespace App\Models;

use App\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Permissions\HasPermissionsTrait;

class User extends Authenticatable implements JWTSubject //, MustVerifyEmail
{
    use Notifiable;
    use HasPermissionsTrait; //Import The Trait

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 
        'last_name', 
        'email', 
        'password',
        'street',
        'street_no',
        'zip',
        'city',
        'phone',
        'image',
        'course_id',
        'customer_id',

        'uid',
        'membership_number',
        'status',
        'salutation',
        'residence',
        'address',
        'birthday',
        'age',
        'initial_contract',
        'actual_contract',
        'duration',
        'end',
        'payment_method',
        'contribution',
        'booking_day',
        'admission',
        'cycle',
        'termination_date',
        'iban',
        
        'points',
        'start_date',
        'end_date',
        'rank_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'roles' => 'array',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'photo_url',
    ];

    /**
     * Get the profile photo URL attribute.
     *
     * @return string
     */
    public function getPhotoUrlAttribute()
    {
        $img = 'https://www.gravatar.com/avatar/'.md5(strtolower($this->email)).'.jpg?s=200&d=mm';

        if(!is_null($this->image))
        {
            $img = url('/img/lg/'.$this->image);
        }
        
        return $img;
    }

    /**
     * Get the oauth providers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function oauthProviders()
    {
        return $this->hasMany(OAuthProvider::class);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    /**
     * @return int
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

     /***
     * @param string $role
     * @return $this
     */
    public function addRole(string $role)
    {
        $roles = $this->getRoles();
        $roles[] = $role;
        
        $roles = array_unique($roles);
        $this->setRoles($roles);

        return $this;
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles)
    {
        $this->setAttribute('roles', $roles);
        return $this;
    }

    /***
     * @param $role
     * @return mixed
     */
    public function hasRole($role)
    {
        return in_array($role, $this->getRoles());
    }

    /***
     * @param $roles
     * @return mixed
     */
    public function hasRoles($roles)
    {
        $currentRoles = $this->getRoles();
        foreach($roles as $role) {
            if ( ! in_array($role, $currentRoles )) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->getUserRoles('roles');

        if (is_null($roles)) {
            $roles = [];
        }

        return $roles;
    }

    public function course()
    {
        return $this->hasOne(Course::class,'id','course_id')->with('facility');
    }
    
    public function rank()
    {
        return $this->hasOne(Rank::class,'id','rank_id')->with('division');
    }

    public function trainingLogs()
    {
        return $this->hasMany(TrainingLog::class,'user_id','id');
    }

    public function examLogs()
    {
        return $this->hasMany(ExamLog::class,'user_id','id')->with('category');
    }

    public function totalPoints()
    {
        return TrainingLog::where([
            ['user_id','=',$this->id],
            ['course_id','=',$this->course_id]
        ])->sum('points');
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class);
    }
}
