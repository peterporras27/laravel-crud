<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 
        'facility_id',
        'city_id',
        'rank_id',
    ];

    public function facility(){
    	return $this->hasOne(Facility::class,'id','facility_id');
    }

    public function employee(){
        return $this->hasOne(User::class,'id','employee_id');
    }

    public function rank(){
        return $this->hasOne(Rank::class,'id','rank_id')->with('division');
    }

    public function schedules(){
        return $this->hasMany(TrainingHour::class,'course_id','id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
