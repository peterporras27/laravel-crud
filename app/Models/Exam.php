<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = [
        'name', 
        'requirements', 
        'rank_id', 
        'category_id', 
        'course_id', 
    ];

    protected $casts = [
        'requirements' => 'array'
    ];

    public function rank(){
        return $this->hasOne(Rank::class, 'id', 'rank_id')->with('division');
    }

    public function course(){
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function category(){
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
