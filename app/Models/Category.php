<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'division_id',
        'min_points'
    ];

    public function division(){
        return $this->hasOne(Division::class,'id','division_id');
    }
}
