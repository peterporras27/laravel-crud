<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingLog extends Model
{
    protected $fillable = [
        'login_time', 
        'course_id', 
        'user_id', 
        'points'
    ];

    public function trainee(){
        return $this->hasOne(User::class,'id','user_id');
    }
}
