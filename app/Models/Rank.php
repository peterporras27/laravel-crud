<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    protected $fillable = [
        'name', 
        'rank', 
        'color', 
        'duration', 
        'max_points', 
        'division_id' 
    ];

    public function division()
    {
    	return $this->hasOne(Division::class,'id','division_id');
    }
}
