<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'belt',
        'status',
        'user_id',
        'prev_rank_id',
        'rank_id',
    ];

    public function prev()
    {
    	return $this->hasOne(Rank::class,'id','prev_rank_id')->with('division');
    }

    public function next()
    {
    	return $this->hasOne(Rank::class,'id','rank_id')->with('division');
    }

    public function user()
    {
    	return $this->hasOne(User::class,'id','user_id');
    }
}
