<?php

namespace App\Imports;

use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;

class UsersImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {

        $r = 0;
        $role = Role::where('slug','customer')->first();

        foreach ($rows as $row) 
        {

            if ($r!=0) 
            {
                $u = explode(";", $row[0]);
                $email = str_replace('"', '', $u[9]);

                if (filter_var($email, FILTER_VALIDATE_EMAIL)) 
                {
                    $exist = User::where('email','=',$email)->first();

                    if (!$exist) 
                    {
                        $u = $this->cleanArr($u);

                        $user = User::create([
                            'uid' => $u[18],
                            'membership_number' => $u[0],
                            'password' => Hash::make($u[9]),
                            'status' => $u[1],
                            'salutation' => $u[2],
                            'first_name' => $u[3],
                            'last_name' => $u[4],
                            'zip' => $u[5],
                            'residence' => $u[6],
                            'address' => $u[7],
                            'phone' => $u[8],
                            'email' => $email,
                            'birthday' => $u[10],
                            'age' => $u[11],
                            'initial_contract' => $u[12],
                            'actual_contract' => $u[13],
                            'duration' => $u[14],
                            'end' => $u[15],
                            'payment_method' => $u[16],
                            'contribution' => $u[17],
                            'booking_day' => $u[19],
                            'admission' => $u[20],
                            'cycle' => $u[21],
                            'termination_date' => $u[22],
                            'iban' => $u[23]
                        ]);

                        $user->roles()->attach($role);

                        $user = null;
                    }
                }
            }
            $r++;
        }
    }

    public function cleanArr($arr)
    {
        $return = [];
        if (is_array($arr)) 
        {
            foreach ($arr as $a) 
            {
               $return[] = str_replace('"', '', $a);
            }    
        }

        return $return;
    }
}
