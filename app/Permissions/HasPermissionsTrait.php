<?php 

namespace App\Permissions;

use App\Models\Permission;
use App\Models\Role;

trait HasPermissionsTrait {

    public function givePermissionsTo(... $permissions) {

        $permissions = $this->getAllPermissions($permissions);

        if($permissions === null) {
            return $this;
        }

        $this->permissions()->saveMany($permissions);
        return $this;
    }

    public function withdrawPermissionsTo( ... $permissions ) {

        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->detach($permissions);
        return $this;
    }

    public function refreshPermissions( ... $permissions ) {

        $this->permissions()->detach();
        return $this->givePermissionsTo($permissions);
    }

    public function hasPermissionTo($permission) {

        return $this->hasPermission($permission);
    }

    public function getUserRoles( $attr )
    {
        $roles = [];
        foreach ($this->roles()->get() as $role) {
            $roles[] = $role->slug;
        }

        return $roles;
    }

    public function getPermissions()
    {
        $permissions = [];
        foreach ($this->permissions()->get() as $perm) {
            $permissions[] = $perm->slug;
        }

        return $permissions;
    }

    public function roles() {

        return $this->belongsToMany(Role::class,'users_roles');
    }

    public function permissions() {

        return $this->belongsToMany(Permission::class,'users_permissions');

    }
    protected function hasPermission($permission) {
        
        return (bool) $this->permissions->where('slug', $permission->slug)->count();
    }

    protected function getAllPermissions(array $permissions) {

        return Permission::whereIn('slug',$permissions)->get();

    }

}